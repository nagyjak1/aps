beq x0,x0,main

# merge
# Inputs: a0 - address of first picture
#	  a1 - address of second picture
#	  a2 - address where to save result picture
# Outputs: a0 - number of pixels in result picture
#
merge:
addi a3,x0,0 # number of result's picture pixels

lw t1,0(a0) # load picture header to t1
sw t1,0(a2) # save picture header to result's picture address

lw t1,4(a0) # load picture width to t1
sw t1,4(a2) # save result picture width at result's picture address+4

lw t2,8(a0) # load picture height to t1
sw t2,8(a2) # save result picture height at result's picture address+8

addi a0,a0,12 
addi a1,a1,12
addi a2,a2,12 # set all picture's addresses to the start of their pixels segment

# t1 - picture width
# t2 - picture height
# t3 - outer for cycle for picture's height
# t4 - inner for cycle for picture's width
addi t3,x0,0

for1:
beq t3,t2,end
addi t3,t3,1
addi t4,x0,0

for2:
beq t4,t1,for1
addi t4,t4,1


lw t5,0(a0)  # t5 = first picture's pixel
lw t6,0(a1)  # t6 = second picture's pixel
addu.qb t0,t5,t6 # add pixels together byte by byte 

# alpha change
lui t5,0x00FFF # t5 = 0x00FFF000
addi t6,x0,0x7FF # t6 = 0x000007FF
addi t6,t6,0x700 # t6 = 0x00000EFF
addi t6,t6,0x100  # t6 = 0x00000FFFF
add t5,t5,t6 # t5 = 0x00FFFFFF

and t0,t5,t0  # t0 = 0x00CCCCCC ( C = correct ) 

lui t5,0xFF000 # t5 = 0xFF000000
add t0,t0,t5 # t5 = 0xFFCCCCCC

sw t0,0(a2) 

addi a0,a0,0x4
addi a1,a1,0x4
addi a2,a2,0x4   # move addresses to next pixel 
addi a3,a3,1 # increment number of changed pixels

j for2

end:
add a0,x0,a3  # return a0
ret

main:
lw a0,0x4(x0)
lw a1,0x8(x0)
lw a2,0xC(x0)
jal merge
sw a0,0x10(x0)
finish:
beq x0,x0 finish
