`default_nettype none

module processor(input clk, reset,
    output[31:0] PC,
    input[31:0] instruction,
    output WE,
    output[31:0] address_to_mem,
    output[31:0] data_to_mem,
    input[31:0] data_from_mem);

    wire[31:0] regDataOut1, regDataOut2;
    wire[31:0] regDataIn, regDataInFirst;
    wire[2:0] ALUControl;
    wire[2:0] ImmDecodeControl;
    wire MemWrite, MemToReg, ALUSrc, RegWrite, BranchJalr, BranchJal, BranchBeq, PCToAlu, ImmIsResult;
    wire[31:0] ImmOperand;
    wire[31:0] AluDataIn2, AluDataIn1;
    wire zero;
    wire[31:0] ALUResult;
    wire[31:0] DataIntoRegister;

    wire[31:0] PCPlus4, PCPlusImm;
    wire[31:0] newPC;
    reg [31:0] regPC;

    PCAdder PCPlus4Adder(regPC, PCPlus4);

    JmpAddressAdder PCImmAdder(regPC, ImmOperand, PCPlusImm);

    BranchControl BranchLogicControl(BranchJalr, BranchBeq, BranchJal, zero, PCPlusImm, PCPlus4, ALUResult, newPC);

    ControlUnit controlUnit(instruction[31:0], ALUControl, ImmDecodeControl, MemWrite, MemToReg, ALUSrc, RegWrite, BranchJalr, BranchJal, BranchBeq, PCToAlu, ImmIsResult);

    ImmDecodeUnit ImmDecodeUnit(instruction[31:0], ImmDecodeControl, ImmOperand);

    register32 register(instruction[19:15], instruction[24:20], instruction[11:7], clk, RegWrite, regDataIn, regDataOut1, regDataOut2);

    ALU ALU(ALUControl, AluDataIn1, AluDataIn2, zero, ALUResult);

    mux_2_1 AluSrcMux(regDataOut2, ImmOperand, ALUSrc, AluDataIn2);
    mux_2_1 BranchJalJalrMux(ALUResult, PCPlus4, BranchJal | BranchJalr, DataIntoRegister);
    mux_2_1 MemToRegMux(DataIntoRegister, data_from_mem, MemToReg, regDataInFirst);
    mux_2_1 PCToAluMux( regDataOut1, regPC, PCToAlu, AluDataIn1);
    mux_2_1 ImmIsResultMux ( regDataInFirst, ImmOperand, ImmIsResult, regDataIn);

    assign data_to_mem = regDataOut2;
    assign address_to_mem = ALUResult;
    assign WE = MemWrite;

    always@( posedge clk )
        begin
            regPC = reset?0:newPC;
        end

    assign PC = regPC;



endmodule


//-------------------------------------------------------------------------------------------------------

module register32(
    input[4:0] A1, A2, A3,
    input clk, WE3,
    input[31:0] WD3,
    output reg [31:0] RD1, RD2);

    reg[31:0] rf[31:0];
    integer i;
    initial
    begin
        for ( i = 0; i < 32; ++i)
            begin
                rf[i] = 0;
            end
    end

    always @(*)
    begin
        rf[0] = 0;
        RD1 = rf[A1];
        RD2 = rf[A2];
    end

    always @(posedge clk)
        begin
            rf[A3] = WE3 ? WD3:rf[A3];

        end
endmodule

//-------------------------------------------------------------------------------------------------------

module ControlUnit(
    input[31:0] instr,
    output reg[2:0] ALUControl,
    output reg[2:0] ImmControl,
    output reg MemWrite, MemToReg, ALUSrc, RegWrite, BranchJalr, BranchJal, BranchBeq, PCALUSrc, ImmIsResult);

    always @(*)
        case (instr[6:0]) // OPCODE
            7'b0110011:
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUSrc = 0;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'bXXX;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                    case (instr[14:12]) // ADD or AND or SUB or SLT or SLL or SRL or SRA
                        3'b010: // SLT
                            ALUControl = 3'b010;
                        3'b111: // AND
                            ALUControl = 3'b011;
                        3'b000: // ADD or SUB
                            case (instr[31:25])
                                7'b0000000: // ADD
                                    ALUControl = 3'b000;
                                7'b0100000: // SUB
                                    ALUControl = 3'b001;
                            endcase
                        3'b101: // SRL or SRA
                            case (instr[31:25])
                                7'b0000000: // SRL
                                    ALUControl = 3'b110;
                                7'b0100000: // SRA
                                    ALUControl = 3'b111;
                            endcase
                        3'b001: // SLL
                            ALUControl = 3'b101;
                    endcase
                end

            7'b0010011: // ADDI
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'b000;
                    ALUSrc = 1;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b000;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end

            7'b0001011: // ADDUQB
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'b100;
                    ALUSrc = 0;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'bXXX;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end

            7'b1100011: // BEQ
                begin
                    MemWrite = 0;
                    MemToReg = 1'bX;
                    ALUControl = 3'b001;
                    ALUSrc = 0;
                    RegWrite = 0;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 1;
                    ImmControl = 3'b010;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end

            7'b0000011: // LW
                begin
                    MemWrite = 0;
                    MemToReg = 1;
                    ALUControl = 3'b000;
                    ALUSrc = 1;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b000;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end

            7'b0100011: // SW
                begin
                    MemWrite = 1;
                    MemToReg = 0;
                    ALUControl = 3'b000;
                    ALUSrc = 1;
                    RegWrite = 0;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b011;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end

            7'b0110111: // LUI
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'bXXX;
                    ALUSrc = 1;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b001;
                    PCALUSrc = 0;
                    ImmIsResult = 1;
                end
            7'b1101111: // JAL
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'bXXX;
                    ALUSrc = 1'bX;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 1;
                    BranchBeq = 0;
                    ImmControl = 3'b100;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end
            7'b1100111: // JALR
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'b000;
                    ALUSrc = 1;
                    RegWrite = 1;
                    BranchJalr = 1;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b000;
                    PCALUSrc = 0;
                    ImmIsResult = 0;
                end
            7'b0010111: // AUIPC
                begin
                    MemWrite = 0;
                    MemToReg = 0;
                    ALUControl = 3'b000;
                    ALUSrc = 1;
                    RegWrite = 1;
                    BranchJalr = 0;
                    BranchJal = 0;
                    BranchBeq = 0;
                    ImmControl = 3'b001;
                    PCALUSrc = 1;
                    ImmIsResult = 0;
                end
        endcase

endmodule

//-------------------------------------------------------------------------------------------------------

module ImmDecodeUnit(
    input[31:0] instr, input[2:0] ImmControl,
    output reg[31:0] ImmOperand);
    always @(*)
        begin
            case (ImmControl)
                3'b000: // ADDI, JALR, LW
                    ImmOperand = {{20{instr[31]}}, instr[31:20]};
                3'b001: // LUI, AUIPC
                    ImmOperand = {instr[31:12], 12'b0000_0000_0000};
                3'b010: // BEQ
                    ImmOperand = {{20{instr[31]}}, instr[7], instr[30:25], instr[11:8], 1'b0};
                3'b011: // SW
                    ImmOperand = {{20{instr[31]}}, instr[31:25], instr[11:7]};
                3'b100: // JAL
                    ImmOperand = {{12{instr[31]}}, instr[19:12], instr[20], instr[30:21], 1'b0};
            endcase

        end

endmodule

//-------------------------------------------------------------------------------------------------------

module mux_2_1(input[31:0] D1, D2,
    input Select,
    output reg[31:0] DataOut);

    always @(*)
    begin
        if (!Select)
            DataOut = D1;
        else
            DataOut = D2;
    end
endmodule

//-------------------------------------------------------------------------------------------------------

module ALU(input[2:0] ALUControl,
    input[31:0] op1, op2,
    output reg zero,
    output reg[31:0] result);

    always @(*)
        begin
            case (ALUControl)
                3'b000: // +
                    result = op1+op2;
                3'b001: // -
                    result = op1-op2;
                3'b010: // slt - cmp
                    result = $signed(op1) < $signed(op2);
                3'b011: // AND
                    result = op1 & op2;
                3'b100: // ADDU.QB
                begin
                    result[7:0] = op1[7:0]+op2[7:0];
                    result[15:8] = op1[15:8]+op2[15:8];
                    result[23:16] = op1[23:16]+op2[23:16];
                    result[31:24] = op1[31:24]+op2[31:24];
                end
                3'b101: // SLL
                    result = op1 << op2;
                3'b110: // SRL
                    result = op1 >> op2;
                3'b111: // SRA
                    result = $signed(op1) >>> op2;
                default:
                    result = op2;
            endcase

            if (result == 0)
                zero <= 1;
            else
                zero <= 0;

        end

endmodule

//-------------------------------------------------------------------------------------------------------

module PCAdder(input[31:0] PC,
    output[31:0] newPC);

    assign newPC = PC+4;

endmodule

//-------------------------------------------------------------------------------------------------------

module mux_3_1(input[31:0] D1, D2, D3,
    input[1:0] Select,
    output reg[31:0] DataOut);

    always @(*)
        case (Select)
            0: DataOut <= D1;
            1: DataOut <= D2;
            default: DataOut = D3;
        endcase
endmodule

//-------------------------------------------------------------------------------------------------------

module BranchControl(input BranchJalr, BranchBeq, BranchJal, zero,
    input[31:0] AddressPlusImm,
    input[31:0] AddressPlus4,
    input[31:0] AddressPlusReg,
    output reg[31:0] PC);

    always @(*)
    begin
        if (BranchJalr == 1)
            PC <= AddressPlusReg;

        else if ( BranchBeq == 1 && zero == 1 )
            PC <= AddressPlusImm;

        else if ( BranchJal == 1 )
            PC <= AddressPlusImm;

        else
            PC <= AddressPlus4;
    end

endmodule

//-------------------------------------------------------------------------------------------------------

module JmpAddressAdder(input[31:0] operand1, operand2,
    output[31:0] result);

    assign result = operand1+operand2;

endmodule

    `default_nettype wire