#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <math.h>
#include <sstream>

class CPixel {
public:
    u_char R;
    u_char G;
    u_char B;
    CPixel() = default;

    CPixel(u_char red, u_char green, u_char blue)
            : R(red),
              G(green),
              B(blue) {}

    void writePixel(std::fstream &out) const {
        out << R;
        out << G;
        out << B;
    }

    u_char getGreyscale () const {
        double res = round(0.2126*R + 0.7152*G + 0.0722*B);
        return res>=255?255:res;
    }

    void convulate ( CPixel center, CPixel top, CPixel bottom, CPixel right, CPixel left ) {
        int tmp;
        tmp = 5*center.R - top.R - bottom.R - right.R - left.R;
        R = tmp<=0?0:(tmp>=255?255:tmp);

        tmp = 5*center.G - top.G - bottom.G - right.G - left.G;
        G = tmp<=0?0:(tmp>=255?255:tmp);

        tmp = 5*center.B - top.B - bottom.B - right.B - left.B;
        B = tmp<=0?0:(tmp>=255?255:tmp);
    }
};


class CImage {
public:
    CImage()
    : format{},
    width{},
    height{},
    maxVal{},
    pixels{}
    {}

    CImage ( const CImage& src ) {
        format = src.format;
        width = src.width;
        height = src.height;
        maxVal = src.maxVal;
        pixels = src.pixels;
    }


    void loadImage(const std::string &filePath) {

        std::fstream in;
        in.open(filePath, std::ios::in | std::ios::binary );

        if (!in.is_open()) {
            std::cout << "Chyba pri otevirani souboru.\n";
            return;
        }

        std::getline(in, format);
        if (format != "P6") {
            std::cout << "Spatny format obrazku\n";
            return;
        }

        in >> width >> height >> maxVal;
        in.get();

        pixels.resize(height);
        for (auto i : pixels)
            i.resize(width);

        for (auto i = 0; i < height; ++i) {
            for (auto j = 0; j < width; ++j) {
                char buffer [3];
                in.read( buffer, 3);
                CPixel pixel ( buffer[0], buffer[1], buffer[2]);
                pixels[i].push_back( pixel );
            }
        }

        in.close();
    }

    void saveImage(const std::string &filePath) {
        std::fstream out;
        out.open(filePath, std::ios::out | std::ios::binary );

        if (!out.is_open()) {
            std::cout << "Soubor pro vystup nelze otevrit\n" << std::endl;
            return;
        }

        out << format << std::endl << width << std::endl << height << std::endl << maxVal << std::endl;

        for (auto i = 0; i < height; ++i) {
            for (auto j = 0; j < width; ++j) {
                pixels[i][j].writePixel(out);
            }
        }

        out.close();
    }

    CImage convolute() const {
        CImage result ( *this );

        for ( auto i = 0; i < height; ++i ) {
            for ( auto j = 0; j < width; ++j ) {
                if ( i == 0 || j == 0 || j == width-1 || i == height-1 )
                    continue;
                result.pixels[i][j].convulate ( pixels[i][j], pixels[i-1][j], pixels[i+1][j], pixels[i][j+1], pixels[i][j-1] );
            }
        }
        return result;
    }

    void getHistogram( const std::string & filepath ) const {
        std::stringstream result;
        int intervals [5] {0};
        u_char tmp;

        for ( auto & i : pixels ) {
            for ( auto & j : i ) {
                tmp = j.getGreyscale();
                if ( tmp <= 50 )
                    intervals[0]++;
                else if ( tmp <= 101 )
                    intervals[1]++;
                else if ( tmp <= 152 )
                    intervals[2]++;
                else if ( tmp <= 203 )
                    intervals[3]++;
                else
                    intervals[4]++;
            }
        }

        std::fstream outfile;
        outfile.open( filepath, std::ios::out );

        if ( ! outfile.is_open() ) {
            std::cout << "Soubor pro vystup se nepodarilo otevrit.\n" << std::endl;
            return;
        }

        outfile << intervals[0] << " " << intervals[1] << " " << intervals[2] << " " << intervals[3] << " " << intervals[4];
        outfile.close();

    }

    std::string format;
    int width;
    int height;
    int maxVal;
    std::vector<std::vector<CPixel>> pixels;
};


int main(int argc, char **argv) {
    CImage image;

    image.loadImage( argv[1] );

    CImage changedImg;
    changedImg = image.convolute();
    changedImg.saveImage( "output.ppm");
    changedImg.getHistogram( "output.txt");

    return 0;
}
